var xhr = new XMLHttpRequest();
xhr.open("GET", "http://billing.enterix.ru/getNotifications?action=getNotifications&login=" + localStorage['login'] + "&password=" + localStorage['password'], true);
xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        //sources=1;support=1;documents=2
        var result = xhr.responseText;
        if (result == -1) {
            info = "<b>Внимание!</b><br/>Необходимо ввести корректную пару логин+пароль в параметрах расширения.";
        } else {
            var a = result.split(';');
            for (var i = 0; i < a.length; i++) {
                var x = a[i].split('=');
                if (x[0] == 'sources') {
                    localStorage['countSourcesNotifications'] = x[1];
                } else if (x[0] == 'support') {
                    localStorage['countSupportNotifications'] = x[1];
                } else if (x[0] == 'documents') {
                    localStorage['countDocumentsNotifications'] = x[1];
                }
            }
            var count = parseInt(localStorage['countSourcesNotifications']) + parseInt(localStorage['countSupportNotifications']) + parseInt(localStorage['countDocumentsNotifications']);
            if (count > 0) {
                info = "<b>Enterix</b><br/>";
                var c = parseInt(localStorage['countSourcesNotifications']);
                if (c > 0) {
                    if ((c % 10 == 1) & (c !== 11)) {
                        info += "<br/><b>" + c + "</b> подпись на модерации";
                    } else {
                        if ((c > 1) & (c < 5)) {
                            info += "<br/><b>" + c + "</b> подписи на модерации";
                        } else {
                            info += "<br/><b>" + c + "</b> подписей на модерации";
                        }
                    }
                }
                var c = parseInt(localStorage['countSupportNotifications']);
                if (c > 0) {
                    if ((c % 10 == 1) & (c !== 11)) {
                        info += "<br/><b>" + c + "</b> событие в техподдержке";
                    } else {
                        if ((c > 1) & (c < 5)) {
                            info += "<br/><b>" + c + "</b> события в техподдержке";
                        } else {
                            info += "<br/><b>" + c + "</b> событий в техподдержке";
                        }
                    }
                }
                var c = parseInt(localStorage['countDocumentsNotifications']);
                if (c > 0) {
                    if ((c % 10 == 1) & (c !== 11)) {
                        info += "<br/><b>" + c + "</b> событие в документообороте";
                    } else {
                        if ((c > 1) & (c < 5)) {
                            info += "<br/><b>" + c + "</b> события в документообороте";
                        } else {
                            info += "<br/><b>" + c + "</b> событий в документообороте";
                        }
                    }
                }
            } else {
                info = "В кабинете Enterix нет новых событий.";
            }
            document.getElementById('popupInfo').innerHTML = info;
            if (count > 0) {
                chrome.browserAction.setBadgeText({
                    text: count + ""
                });
            } else {
                chrome.browserAction.setBadgeText({
                    text: ""
                });
            }
        }
    }
}
xhr.send();

//Листенер - если пользователь кликнет на кнопку в окне расширения, то откроется ЛК на новой вкладке
document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('linkToBilling').onclick = function() {
        chrome.tabs.create({
            url: "http://billing.enterix.ru/?user=" + localStorage['login'] + "&password=" + localStorage['password']
        });
    };
});
