//Сохраняем параметры расширения
function save_options() {
    localStorage['login'] = document.getElementById("login").value;
    localStorage['password'] = document.getElementById("password").value;
    localStorage['refreshSources'] = parseFloat(document.getElementById("refreshSources").value);
    localStorage['refreshSupport'] = parseFloat(document.getElementById("refreshSupport").value);
    localStorage['refreshDocuments'] = parseFloat(document.getElementById("refreshDocuments").value);    
    localStorage['timeCounter'] = 0;

    //Показываем пользователю, что настройки сохранены
    var status = document.getElementById("status");
    status.innerHTML = "Парметры успешно сохранены!";
    setTimeout(function() {
        document.getElementById("status").innerHTML = "";
    }, 2750);

    //Посылаем запрос на background.html
    chrome.extension.sendMessage({
        do: "update"
    }, function(response) {
        //console.log(response);
    });
}

//Восстанавливаем значения из localStorage
function restore_options() {
    document.getElementById("login").value = localStorage["login"];
    document.getElementById("password").value = localStorage["password"];
    document.getElementById("refreshSources").value = localStorage["refreshSources"];
    document.getElementById("refreshSupport").value = localStorage["refreshSupport"];
    document.getElementById("refreshDocuments").value = localStorage["refreshDocuments"];
}

document.addEventListener('DOMContentLoaded', function() {
    restore_options();
    document.querySelector('#form').addEventListener('submit', function(event) {
        save_options();
        event.preventDefault();
    });
});
