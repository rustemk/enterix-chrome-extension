var interval;
//Функция для запуска, которая через выставленное время в настройках будет запускать функцию main. Также запускается при измении времени обновления.
function init() {
    window.clearInterval(interval)
    interval = window.setInterval(function() {
        main();
    }, 60000);
}

//Главная функция, делает запрос в биллинг и выдает всплывающие окошки при необходимости
function main() {
    if (parseInt(localStorage['timeCounter']) > 1440) {
        localStorage['timeCounter'] = 0;
    }
    localStorage['timeCounter'] = parseInt(localStorage['timeCounter']) + 1;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://billing.enterix.ru/getNotifications?action=getNotifications&login=" + localStorage['login'] + "&password=" + localStorage['password'], true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            //sources=1;support=1;documents=2
            var result = xhr.responseText;
            if (result == -1) {
                showNotification("Уведомление от Enterix", "Внимание! Необходимо ввести корректную пару логин+пароль в параметрах расширения.");
            } else {
                var a = result.split(';');
                for (var i = 0; i < a.length; i++) {
                    var x = a[i].split('=');
                    if (x[0] == 'sources') {
                        localStorage['countSourcesNotifications'] = x[1];
                    } else if (x[0] == 'support') {
                        localStorage['countSupportNotifications'] = x[1];
                    } else if (x[0] == 'documents') {
                        localStorage['countDocumentsNotifications'] = x[1];
                    }
                }
                if (parseInt(localStorage['timeCounter']) % parseInt(localStorage['refreshSources']) == 0) {
                    var count = parseInt(localStorage['countSourcesNotifications']);
                    if (count > 0) {
                        var audio = new Audio("alert1.wav");
                        audio.play();
                        if ((count % 10 == 1) & (count !== 11)) {
                            showNotification("Уведомление от Enterix", "В личном кабинете Enterix " + count + " подпись ожидает модерации!");
                        } else {
                            if ((count > 1) & (count < 5)) {
                                showNotification("Уведомление от Enterix", "В личном кабинете Enterix " + count + " подписи ожидает модерации!");
                            } else {
                                showNotification("Уведомление от Enterix", "В личном кабинете Enterix " + count + " подписей ожидают модерации!");
                            }
                        }
                    }
                }
                if (parseInt(localStorage['timeCounter']) % parseInt(localStorage['refreshSupport']) == 0) {
                    var count = parseInt(localStorage['countSupportNotifications']);
                    if (count > 0) {
                        var audio = new Audio("alert1.wav");
                        audio.play();
                        if ((count % 10 == 1) & (count !== 11)) {
                            showNotification("Уведомление от Enterix", "В техподдержке Enterix " + count + " новое событие!");
                        } else {
                            if ((count > 1) & (count < 5)) {
                                showNotification("Уведомление от Enterix", "В техподдержке Enterix " + count + " новых события!");
                            } else {
                                showNotification("Уведомление от Enterix", "В техподдержке Enterix " + count + " новых событий!");
                            }
                        }
                    }
                }
                if (parseInt(localStorage['timeCounter']) % parseInt(localStorage['refreshDocuments']) == 0) {
                    var count = parseInt(localStorage['countDocumentsNotifications']);
                    if (count > 0) {
                        var audio = new Audio("alert1.wav");
                        audio.play();
                        if ((count % 10 == 1) & (count !== 11)) {
                            showNotification("Уведомление от Enterix", "В документообороте Enterix " + count + " новое событие!");
                        } else {
                            if ((count > 1) & (count < 5)) {
                                showNotification("Уведомление от Enterix", "В документообороте Enterix " + count + " новых события!");
                            } else {
                                showNotification("Уведомление от Enterix", "В документообороте Enterix " + count + " новых событий!");
                            }
                        }
                    }
                }
                var count = parseInt(localStorage['countSourcesNotifications']) + parseInt(localStorage['countSupportNotifications']) + parseInt(localStorage['countDocumentsNotifications']);
                if (count > 0) {
                    chrome.browserAction.setBadgeText({
                        text: count + ""
                    });
                } else {
                    chrome.browserAction.setBadgeText({
                        text: ""
                    });
                }
            }
        }
    }
    xhr.send();
}

//Хелпер для показа всплывающего окна
function showNotification(title, text) {
    var notification = chrome.notifications.create('', {
        iconUrl: 'envelope.png',
        type: 'basic',
        title: title,
        message: text
    }, function(notification_id) {
        window.setTimeout(function() {
            chrome.notifications.clear(notification_id);
        }, 10000);
    });
}

//Листенер - если пользователь кликнет на всплывшее окошко, то откроется ЛК на новой вкладке
chrome.notifications.onClicked.addListener(function(notification_id, byUser) {
    chrome.tabs.create({
        url: "http://billing.enterix.ru/?user=" + localStorage['login'] + "&password=" + localStorage['password']
    });
});

//Задаем наш фирменный цвет фону баджей
chrome.browserAction.setBadgeBackgroundColor({
    color: "#B55013"
});
