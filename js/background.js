//Поставить дефолтные значения при первом запуске
if (!localStorage['login']) {
    localStorage['login'] = "-";
}
if (!localStorage['password']) {
    localStorage['password'] = "-";
}
if (!localStorage['refreshSources']) {
    localStorage['refreshSources'] = 5;
}
if (!localStorage['refreshSupport']) {
    localStorage['refreshSupport'] = 5;
}
if (!localStorage['refreshDocuments']) {
    localStorage['refreshDocuments'] = 5;
}
if (!localStorage['timeCounter']) {
    localStorage['timeCounter'] = 0;
}
if (!localStorage['countSupportNotifications']) {
    localStorage['countSupportNotifications'] = 0;
}
if (!localStorage['countSourcesNotifications']) {
    localStorage['countSourcesNotifications'] = 0;
}
if (!localStorage['countSupportNotifications']) {
    localStorage['countSupportNotifications'] = 0;
}

chrome.extension.onMessage.addListener(
    function(request, sender, sendResponse) {
        //Проверка, что данные пришли со страницы опций этого же плагина и параметр do равен update
        if (sender.tab.url == chrome.extension.getURL("options.html") && request.do == "update") {
            main();
            init();
            sendResponse({
                status: "ok"
            });
        }
    }
);

main();
init();
